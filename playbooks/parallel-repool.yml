---
- name: Parallel Repool of MariaDB Hosts
  hosts: localhost
  gather_facts: false
  vars:
    task_id: "{{ task_id | default('T12345') }}"
    pool_speed: "{{ pool_speed | default('') }}"
    host_list: "{{ hosts.split(',') }}"
    tmux_session_name: "repools-{{ task_id }}-{{ ansible_date_time.epoch }}"
  tasks:
    - name: Create tmux session
      ansible.builtin.command: >
        tmux new-session -d -s "{{ tmux_session_name }}"
      changed_when: false
      check_mode: false
      delegate_to: "{{ cumin_host }}"

    - name: Debug - Create tmux session
      ansible.builtin.debug:
        msg: "Would run: tmux new-session -d -s {{ tmux_session_name }}"
      when: ansible_check_mode

    - name: Set tmux remain-on-exit option
      ansible.builtin.command: >
        tmux set-option -t "{{ tmux_session_name }}" remain-on-exit on
      changed_when: false
      check_mode: false
      delegate_to: "{{ cumin_host }}"

    - name: Debug - Set tmux remain-on-exit option
      ansible.builtin.debug:
        msg: "Would run: tmux set-option -t {{ tmux_session_name }} remain-on-exit on"
      when: ansible_check_mode

    - name: Repool hosts in parallel
      ansible.builtin.command: >
        tmux split-window -t "{{ tmux_session_name }}"
        "sleep {{ 5 | random(10) }} &&
         sudo cookbook sre.mysql.repool
         --query {{ item }}
         -r 'Parallel repool'
         -t {{ task_id }}
         {% if pool_speed == 'fast' %}--fast{% elif pool_speed == 'slow' %}--slow{% endif %}"
      loop: "{{ host_list }}"
      async: 0
      poll: 0
      changed_when: false
      when: not ansible_check_mode
      delegate_to: "{{ cumin_host }}"

    - name: Debug - Repool hosts in parallel
      ansible.builtin.debug:
        msg: >
          "Would run for host {{ item }}:
          tmux split-window -t {{ tmux_session_name }}
          'sleep {{ 5 | random(10) }} &&
           sudo cookbook sre.mysql.repool
           --query {{ item }}
           -r 'Parallel repool'
           -t {{ task_id }}
           {% if pool_speed == 'fast' %}--fast{% elif pool_speed == 'slow' %}--slow{% endif %}'"
      loop: "{{ host_list }}"
      when: ansible_check_mode

    - name: Set tmux layout to tiled
      ansible.builtin.command: >
        tmux select-layout -t "{{ tmux_session_name }}" tiled
      changed_when: false
      check_mode: false
      delegate_to: "{{ cumin_host }}"

    - name: Debug - Set tmux layout to tiled
      ansible.builtin.debug:
        msg: "Would run: tmux select-layout -t {{ tmux_session_name }} tiled"
      when: ansible_check_mode

    - name: Display information about the tmux session
      ansible.builtin.debug:
        msg:
          - "Parallel repool started in tmux session: {{ tmux_session_name }}"
          - "You can attach to the session with: tmux attach-session -t {{ tmux_session_name }}"
      when: not ansible_check_mode

    - name: Debug - Display information about the tmux session
      ansible.builtin.debug:
        msg:
          - "Would start parallel repool in tmux session: {{ tmux_session_name }}"
          - "You would be able to attach to the session with: tmux attach-session -t {{ tmux_session_name }}"
      when: ansible_check_mode
