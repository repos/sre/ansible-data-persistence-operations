# copy mariadb host

This role is a work in progress as a general glue code to copy mariadb instances from one host to another.

# Good to know

Some [modules](https://docs.ansible.com/ansible/latest/collections/community/mysql/index.html) don't support dry run. 
To workaround that issue,  `ansible-playbook copy_mariadb_host.yml -e 'force_dry_run=true' -D` without `--check` will help you debug commands involving those modules. Be sure to double check ansible.builtin.commands wont run when debugging.
