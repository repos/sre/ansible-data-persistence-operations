# ansible-data-persistence-operations

## What this repository is not
* a reimplementation of already existing Cumin commands, Cookbooks, or Puppet Roles

## What this repository is
* a quick mean to execute data persistence operations based on existing Cumin commands, Cookbooks, and Puppet Roles
* a decent way to avoid human mistakes at runtime
* a pattern for a future implementation of a set of cookbooks and roles to fill the same gaps Ansible is filling via this repo.

# What's been done
The first role is designed to allow easy execution of the [cloning process]( https://wikitech.wikimedia.org/wiki/MariaDB/Clone_a_host) once Puppet Hieradata has been setup properly. It's a very simple role, but it's a good starting point to understand how to use this repository.